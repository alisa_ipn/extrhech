import os 
import sys

###################################################################################
#  gets back a tagged sentence splitted word-per-line (after Freeling) back to line
#
#  USAGE: 
#  //tagged_back_2_line.py splitted_POStagged_file_entire_path  path_to_outpt_folder
#    tagged_back2line_whole_file.py POStagged_file file_for_normal_text_sent_per_line >POS_tagged sents in line 
#
#  OUTPUT: 
#  name of teh output files stays the same as of teh input. Presumabely, 
#  path_to_outpt_folder is different. 
###################################################################################


#OUTEXT = '_LINED.pos'
#OUTPATH ="D:/Study/OpenInfoExtraction/50docs/numbered/pos/lined/"
#OUTPATH ="D:/Study/OpenInfoExtraction/dataset/POS-tagged/doubleUTF8_lined/"
OUTFN = "D:/Study/OpenInfoExtraction/datasets/reut_latam_1000_preps2.txt"

# not used
'''
def get_labels(path):  
    labels = {}
    print "Reading pair labels ..."
    # for each subcategory file in a directory 
    for fi in os.listdir(path):
        #print fi
        full_name = os.path.join(path, fi)
        fnPairs = full_name
        ch = fi[fi.rfind('-')+1:fi.find('.txt')]
        for ln in open(fnPairs):
            pair = ln.strip()
            if pair not in labels: 
              labels[pair] = [ch]
            else:  
              labels[pair].append(ch)
    
    return labels 
'''


def main(argv):
    if len(argv) != 2:
    #if len(argv) != 3:
        sys.stderr.write("Usage: %s POStagged_file  OUT_File" % argv[0])
        sys.exit(-1)
    
    filenm = argv[1]
    #outfn = argv[2]
    #print file_path
    
    #lines = []     
    sent = []
    #sent_no_tag = []
    empt_ln = 0  
    #fout = open(outfn, 'w')
    for s in open(filenm):  
      if s.strip() == '':
        #print "This is an empty line" 
        empt_ln += 1
        if sent !=[]:
          print ' '.join(s.strip() for s in sent if len(s.strip()) != 0)
          #print >>fout, ' '.join(wd.strip() for wd in sent_no_tag if len(wd.strip()) != 0)
        sent = []
        #sent_no_tag = []
        continue 
      ws = s.strip().split()
      #print s 
      #print ws 
      word = ws[0]+'^'+ws[1]+'^'+ws[2]
      #word_no_tag = ws[0].strip()
      sent.append(word)
      #sent_no_tag.append(word_no_tag)
      #print sent
    print "total of empty lines", empt_ln

    #fout.close()   
     

    #labels = get_labels(path)
    """
    print "Printing out the results to: ", OUTNM
    fout = open(OUTNM, 'w')
    for pair in labels: 
         print >>fout, pair, '\t', labels[pair]
    fout.close() 
    """

if __name__ == "__main__":
    main(sys.argv)