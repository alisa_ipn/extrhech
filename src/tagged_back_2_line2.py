import os 
import sys

###################################################################################
#  gets back a tagged sentence splitted word-per-line (after Freeling) back to line
#
#  USAGE: 
#  tagged_back_2_line.py splitted_POStagged_file_entire_path  path_to_outpt_folder
#
#  OUTPUT: 
#  name of teh output files stays the same as of teh input. Presumabely, 
#  path_to_outpt_folder is different. 
###################################################################################


#OUTEXT = '_LINED.pos'
#OUTPATH ="D:/Study/OpenInfoExtraction/50docs/numbered/pos/lined/"
#OUTPATH ="D:/Study/OpenInfoExtraction/dataset/POS-tagged/doubleUTF8_lined/"

# not used
'''
def get_labels(path):  
    labels = {}
    print "Reading pair labels ..."
    # for each subcategory file in a directory 
    for fi in os.listdir(path):
        #print fi
        full_name = os.path.join(path, fi)
        fnPairs = full_name
        ch = fi[fi.rfind('-')+1:fi.find('.txt')]
        for ln in open(fnPairs):
            pair = ln.strip()
            if pair not in labels: 
              labels[pair] = [ch]
            else:  
              labels[pair].append(ch)
    
    return labels 
'''


def main(argv):
    if len(argv) != 3:
        sys.stderr.write("Usage: %s POStagged_file_path  output_path " % argv[0])
        sys.exit(-1)
    
    file_path = argv[1]
    outpath = argv[2]
    #print file_path
    
    filenm = os.path.split(file_path)[1]
    #print 'IN: ', filenm
    
    filenm_out = outpath + filenm
    #print  'OUT: ', filenm_out
    
    fout = open(filenm_out, 'w')
    sent = []
    for s in open (file_path):  
      if s.strip() == '':
        #print "This is an empty line" 
        if sent !=[]:
          print >> fout, ' '.join(s.strip() for s in sent if len(s.strip()) != 0)
        sent = []
        continue 
      ws = s.strip().split()
      #print s 
      #print ws 
      word = ws[0]+'^'+ws[1]+'^'+ws[2]
      sent.append(word)
      #print sent

    fout.close()   
     

    #labels = get_labels(path)
    """
    print "Printing out the results to: ", OUTNM
    fout = open(OUTNM, 'w')
    for pair in labels: 
         print >>fout, pair, '\t', labels[pair]
    fout.close() 
    """

if __name__ == "__main__":
    main(sys.argv)